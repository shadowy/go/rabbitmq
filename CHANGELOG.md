<a name="unreleased"></a>
## [Unreleased]


<a name="v1.0.8"></a>
## [v1.0.8] - 2019-10-23
### Features
- add destinations for output


<a name="v1.0.7"></a>
## [v1.0.7] - 2019-08-27
### Bug Fixes
- fix issue when queueName is null and name == ""


<a name="v1.0.6"></a>
## [v1.0.6] - 2019-08-27
### Bug Fixes
- fix issue when queueName is null and name == ""


<a name="v1.0.5"></a>
## [v1.0.5] - 2019-08-23
### Features
- add method SendWithHeader


<a name="v1.0.4"></a>
## [v1.0.4] - 2019-07-25
### Code Refactoring
- remove queue creation


<a name="v1.0.3"></a>
## [v1.0.3] - 2019-07-18

<a name="v1.0.2"></a>
## [v1.0.2] - 2019-07-18

<a name="v1.0.1"></a>
## v1.0.1 - 2019-07-17

[Unreleased]: https://gitlab.com/shadowy/go/rabbitmq/compare/v1.0.8...HEAD
[v1.0.8]: https://gitlab.com/shadowy/go/rabbitmq/compare/v1.0.7...v1.0.8
[v1.0.7]: https://gitlab.com/shadowy/go/rabbitmq/compare/v1.0.6...v1.0.7
[v1.0.6]: https://gitlab.com/shadowy/go/rabbitmq/compare/v1.0.5...v1.0.6
[v1.0.5]: https://gitlab.com/shadowy/go/rabbitmq/compare/v1.0.4...v1.0.5
[v1.0.4]: https://gitlab.com/shadowy/go/rabbitmq/compare/v1.0.3...v1.0.4
[v1.0.3]: https://gitlab.com/shadowy/go/rabbitmq/compare/v1.0.2...v1.0.3
[v1.0.2]: https://gitlab.com/shadowy/go/rabbitmq/compare/v1.0.1...v1.0.2
