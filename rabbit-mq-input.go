package rabbitmq

import (
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

// Input - input stream
type Input struct {
	BaseStream

	Stream <-chan amqp.Delivery
}

// Init - init stream
func (input *Input) Init(prefetchCount, prefetchSize *int) (err error) {
	logrus.Debug("queue.Input.Init")
	if prefetchCount != nil || prefetchSize != nil {
		count := 1
		size := 0
		if prefetchCount != nil {
			count = *prefetchCount
		}
		if prefetchSize != nil {
			size = *prefetchSize
		}
		err = input.channel.Qos(count, size, false)
		if err != nil {
			logrus.WithError(err).Error("queue.Input.Init Qos")
		}
	}
	// Create consumer
	input.Stream, err = input.channel.Consume(input.Name, "", false, false, false, false, nil)
	if err != nil {
		logrus.WithError(err).Error("queue.Input.Init consume")
	}
	return
}
