package rabbitmq

import (
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

// BaseStream - basic class for rabbit-mq
type BaseStream struct {
	Name       string
	Exchange   string
	connection *amqp.Connection // Connection to RabbitMq
	channel    *amqp.Channel    // Channel
}

// Open - open channel to RabbitMq
func (stream *BaseStream) Open(connection *RabbitMq, exchange, name string) (err error) {
	logrus.WithFields(logrus.Fields{
		"name":     name,
		"exchange": exchange,
	}).Info("BaseStream.Open")

	stream.Name = name
	stream.Exchange = exchange
	stream.connection, stream.channel, err = connection.GetConnection()
	if err != nil {
		return
	}
	return
}

// Close - close channel and connection to RabbitMq
func (stream *BaseStream) Close() {
	if stream.channel != nil {
		err := stream.channel.Close()
		if err != nil {
			logrus.WithFields(stream.params()).WithError(err).Error("BaseStream.Close")
		}
		stream.channel = nil
	}
	if stream.connection != nil {
		err := stream.connection.Close()
		if err != nil {
			logrus.WithFields(stream.params()).WithError(err).Error("BaseStream.Close")
		}
		stream.connection = nil
	}
}

func (stream *BaseStream) params() logrus.Fields {
	return logrus.Fields{
		"name": stream.Name,
	}
}
