package rabbitmq

import (
	"errors"
	"fmt"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"sync"
)

/**
OutputDestination - output destination
*/
type OutputDestination struct {
	Exchange  string `yaml:"exchange"`  // Exchange
	QueueName string `yaml:"queueName"` // Name of queue
}

// RabbitMq - rabbit-mq configuration and provider stream
type RabbitMq struct {
	Host          string                        `yaml:"host"`          // Host
	Port          int32                         `yaml:"port"`          // Port
	User          string                        `yaml:"user"`          // User
	Password      string                        `yaml:"password"`      // Password
	Exchange      string                        `yaml:"exchange"`      // Exchange
	QueueName     *string                       `yaml:"queueName"`     // Name of queue
	PrefetchSize  *int                          `yaml:"prefetchSize"`  // Prefetch size
	PrefetchCount *int                          `yaml:"prefetchCount"` // Prefetch count
	Destinations  *map[string]OutputDestination `yaml:"destinations"`  // Defined destination

	connectionString string // Connection string
	cacheForOutput   map[string]*Output
	mutexForOutput   *sync.Mutex
}

// GetConnection - get connection to RabbitMq
func (mq *RabbitMq) GetConnection() (connection *amqp.Connection, channel *amqp.Channel, err error) {
	logrus.Info("RabbitMq.GetConnection")
	connection, err = amqp.Dial(mq.GetConnectionString())
	if err != nil {
		logrus.WithError(err).Error("RabbitMq.GetConnection")
		return
	}
	// Open channel
	channel, err = connection.Channel()
	if err != nil {
		logrus.WithError(err).Error("RabbitMq.GetConnection createChannel")
	}
	return
}

// GetConnectionString - get connection string for RabbitMq
func (mq *RabbitMq) GetConnectionString() string {
	if mq.connectionString != "" {
		return mq.connectionString
	}
	mq.connectionString = fmt.Sprintf("amqp://%s:%s@%s:%d/", mq.User, mq.Password, mq.Host, mq.Port)
	return mq.connectionString
}

// Init - initialize properties
func (mq *RabbitMq) Init() error {
	mq.cacheForOutput = make(map[string]*Output)
	mq.mutexForOutput = &sync.Mutex{}
	return nil
}

// GetInput - get input stream
func (mq *RabbitMq) GetInput(name string) (input *Input, err error) {
	logrus.WithFields(logrus.Fields{"name": name}).Info("queue.RabbitMqQueue.GetInput")
	if name == "" && mq.QueueName != nil {
		name = *mq.QueueName
	}
	input = &Input{}
	err = input.Open(mq, mq.Exchange, name)
	if err != nil {
		input = nil
		return
	}
	err = input.Init(mq.PrefetchCount, mq.PrefetchSize)
	if err != nil {
		input = nil
		return
	}
	return
}

// GetOutput - get output stream
func (mq *RabbitMq) GetOutput(name string) (output *Output, err error) {
	logrus.WithFields(logrus.Fields{"name": name}).Info("queue.RabbitMqQueue.GetOutput")
	if name == "" && mq.QueueName != nil {
		name = *mq.QueueName
	}
	return mq.getOutput(mq.Exchange, name)
}

// GetOutputForDestination - get output stream for destination
func (mq *RabbitMq) GetOutputForDestination(destination string) (output *Output, err error) {
	logrus.WithFields(logrus.Fields{"destination": destination}).Info("queue.RabbitMqQueue.GetOutputForDestination")
	if mq.Destinations == nil {
		return nil, errors.New("destinations not defined")
	}
	dest, ok := (*mq.Destinations)[destination]
	if !ok {
		return nil, errors.New("destination '" + destination + "' not defined")
	}
	return mq.getOutput(dest.Exchange, dest.QueueName)
}

func (mq *RabbitMq) getOutput(exchange string, queue string) (output *Output, err error) {
	logrus.WithFields(logrus.Fields{"queue": queue, "exchange": exchange}).
		Debug("queue.RabbitMqQueue.getOutput")
	mq.mutexForOutput.Lock()
	defer mq.mutexForOutput.Unlock()

	key := fmt.Sprintf("%s:%s", exchange, queue)
	if val, ok := mq.cacheForOutput[key]; ok {
		logrus.WithFields(logrus.Fields{"key": key}).Debug("queue.RabbitMqQueue.getOutput use cache")
		output = val
		return
	}
	logrus.WithFields(logrus.Fields{"key": key}).Debug("queue.RabbitMqQueue.getOutput create instance")
	output = &Output{}
	err = output.Open(mq, exchange, queue)
	if err != nil {
		output = nil
		return
	}
	mq.cacheForOutput[key] = output
	return
}
