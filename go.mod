module gitlab.com/shadowy/go/rabbitmq

go 1.12

require (
	github.com/sirupsen/logrus v1.4.2
	github.com/streadway/amqp v0.0.0-20190404075320-75d898a42a94
)
