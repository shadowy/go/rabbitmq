package rabbitmq

import (
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

// Output - output stream
type Output struct {
	BaseStream
}

// Send - send message to queue
func (output *Output) Send(message []byte, priority uint8) (err error) {
	var msg = amqp.Publishing{
		Body:         message,
		DeliveryMode: amqp.Persistent,
		Priority:     priority,
	}
	err = output.channel.Publish(output.Exchange, output.Name, true, false, msg)
	if err != nil {
		logrus.WithFields(logrus.Fields{"body": message, "name": output.Name, "exchange": output.Exchange, "priority": priority}).
			WithError(err).Error("queue.Output.Send")
	}
	return
}

// SendWithHeader - send message to queue
func (output *Output) SendWithHeader(message []byte, header amqp.Table, priority uint8) (err error) {
	var msg = amqp.Publishing{
		Body:         message,
		DeliveryMode: amqp.Persistent,
		Priority:     priority,
		Headers:      header,
	}
	err = output.channel.Publish(output.Exchange, output.Name, true, false, msg)
	if err != nil {
		logrus.WithFields(logrus.Fields{"body": message, "name": output.Name, "exchange": output.Exchange, "priority": priority}).
			WithError(err).Error("queue.Output.SendWithHeader")
	}
	return
}
